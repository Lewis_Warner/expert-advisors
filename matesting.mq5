//+------------------------------------------------------------------+
//|                                                         test.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\Trade.mqh>
#include <Trade\PositionInfo.mqh>
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
//Moving average period 1, smallest
input int maPeriodOne = 8;
//Moving average period 2, largest
input int maPeriodTwo = 13;
//The time frame for the moving average 1 and 2
input ENUM_TIMEFRAMES maTimePeriod = PERIOD_M5;
//the type of moving average used
input ENUM_MA_METHOD maMethod = MODE_SMA;
//Stores the indicator handles
int    maOne, maTwo;
//Stores the indicator values
double maOneValues[], maTwoValues[];
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
  EventSetTimer(1);
    initaliseIndicators();
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
EventKillTimer();
  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
        CPositionInfo cPositionInfo = new CPositionInfo;
      CTrade cTrade = new CTrade;  
  //Checks if there is any open positions
   int totalOpenPositions = PositionsTotal();

    //Gets the MA values and sees if the trends have changed last checked.
   if(CopyBuffer(maOne,0,0,3,maOneValues)!=3)
      return;
   if(CopyBuffer(maTwo,0,0,3,maTwoValues)!=3)
      return;
   //Gets the current and previous ma values and rounds them to 5 decimals
   double previousMaOneValue = NormalizeDouble(maOneValues[1], 5);
   double previousMaTwoValue = NormalizeDouble(maTwoValues[1], 5);
   double currentMaOneValue = NormalizeDouble(maOneValues[0], 5);
   double currentMaTwoValue = NormalizeDouble(maTwoValues[0], 5);
   //Calculates the current trend direction form the two Moving Averages Used
   int sellTrend = getMovingAverageTrend(currentMaOneValue,currentMaTwoValue);
   int previousSellTrend = getMovingAverageTrend(previousMaOneValue,previousMaTwoValue);

//If trend changes then sells the position
   if(totalOpenPositions != 0) {
      Print("Position Closed");
      long positionTicket;
      for(int i = 0; i< totalOpenPositions; i++)
        {
         PositionGetTicket(i);
         PositionGetInteger(POSITION_TICKET, positionTicket);
         cPositionInfo.SelectByTicket(positionTicket);
         //If 5 MA switches from sell to buy in a sell 1 hour trend then close the position
         if(cPositionInfo.PositionType() == POSITION_TYPE_SELL && sellTrend == 1) {
            cTrade.PositionClose(positionTicket); 
         } 
         if(cPositionInfo.PositionType() == POSITION_TYPE_BUY && sellTrend == 2) {
            cTrade.PositionClose(positionTicket); 
         } 
        }
   }
   double bid = SymbolInfoDouble(_Symbol,SYMBOL_BID);
   //If no orders and the current trend is different from the previous then make trade
   if(totalOpenPositions == 0 && sellTrend != previousSellTrend) {
   //theoretically adds a position with the trend in mind
       Print("Open a " + sellTrend + "Order");
       if(sellTrend == 2) {
         openOrder(ORDER_TYPE_SELL, bid + 0.004, bid - 1, 0, TRADE_ACTION_DEAL, 0.06);
       }
       if(sellTrend == 1) {
         openOrder(ORDER_TYPE_BUY, bid - 0.004, bid + 1, 0, TRADE_ACTION_DEAL, 0.06);
       }
   }
   


  }

void OnTimer()
  {

  }
//Opens an Order
void openOrder(ENUM_ORDER_TYPE orderType, double sl, double tp, double price,ENUM_TRADE_REQUEST_ACTIONS action, double volume)
  {

   double bid = SymbolInfoDouble(_Symbol,SYMBOL_BID);
   MqlTradeRequest order= {0};
   MqlTradeResult result= {0};
   if(action != TRADE_ACTION_DEAL) {
   order.price        = price;
   }
   order.symbol       = _Symbol;
   order.action       = action;
   order.type_time    = ORDER_TIME_GTC;
   order.type_filling = SYMBOL_FILLING_FOK;
   order.volume       = volume;
   order.type         = orderType;
   order.sl           = sl;
   order.tp           = tp;
   order.comment      = "Automated Trade";
   int success = OrderSend(order, result);
   int error = GetLastError();
   int i = 0;
  }
//+------------------------------------------------------------------+
//Initalises indicators the program uses
void initaliseIndicators() {
    maOne = iMA(Symbol(),maTimePeriod,maPeriodOne,1,maMethod,PRICE_WEIGHTED);
    maTwo = iMA(Symbol(),maTimePeriod,maPeriodTwo,1,maMethod,PRICE_WEIGHTED);
    ArraySetAsSeries(maOneValues,true);
    ArraySetAsSeries(maTwoValues,true);
}

//Gets the trend of two two values
int getMovingAverageTrend(double value1, double value2) {
   //Buy trend
    if(value1 > value2) {
    return 1;
   } else
   //Sell Trend
   if (value1 < value2) {
    return 2;
   }
   //No trend
   return 0;
}
